Hydrex Pest Control of Ventura is an independently owned and operated company providing services throughout the San Fernando Valley as well as parts of Los Angeles, Marina del Rey and Beverly Hills. Our licensed and experienced technicians are here for your pest-fighting needs!

Address: 2317 Palma Drive, Unit 4, Ventura, CA 93003, USA

Phone: 805-256-7074

Website: https://callhydrex.com
